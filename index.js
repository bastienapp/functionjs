/*
"Michel" -> "Hello Michel"
"Gertrude" -> "Hello Gertrude"
"" -> "Hello World"
*/
// fonction
// une variable ?
// paramètre : ce que l'on entre dans la fonction
function sayHello(name) {
  return `Hello ${name}`; // backticks : interpolation
  // return "Hello " + name; // concaténation
}

// argument : une valeur passée à la fonction
const result = sayHello("Gertrude");

// console.log(result);

/*
19 -> true
50 -> true
17 -> false
16 -> false
2 -> false
18 -> true
*/
function majeur(age) {
  /*
    if (age >= 18) {
        return true;
    } else {
        return false;
    }
    */
  // opérateur ternaire
  // return 'condition' ? 'resultat si condition vraie' : 'résultat si condition fausse';
  // return isClicked === true ? "a cliqué" : "n'a pas cliqué"
  // return age >= 18 ? true : false;

  return age >= 18;
}

const estMajeur = majeur(27);
console.log(estMajeur);

/*
3 -> "positif"
-1 -> "négatif"
12 -> "positif"
-10000-> "négatif"
0 -> ""
*/
// Math.sign

function isPositive(number) {
  if (number > 0) {
    return "positif";
  } else if (number < 0) {
    return "negatif";
  } else {
    return "";
  }
}

const numberIsPositive = isPositive(5);

console.log("5", numberIsPositive);

console.log("-42", isPositive(-42));
console.log("0", isPositive(0));
